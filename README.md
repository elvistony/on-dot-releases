# <img src="https://elvistony.github.io/assets/repo/ondot/logo.png" width=90px> On-Dot-Releases

On Dot - Release Branch for OnDot App - Vidya ERP

### Official Page : [RWdev - Repo - Website](https://elvistony.github.io/projects/ondot/)

<hr> 

#### Latest Release Version : v1.4 (Stable)
<hr>

##### Features
- Enhanced DarkMode
- Repackaged ERP Due Section
- Fixed Crash Issues
- Added Help Tags
- Possibly the Last Update

    *Currently includes <i>ERP Finance Addon</i> along with Due Breakup Functionality

#### Overview
<b>No more FE. </b>Our app helps you keep track of our College Attendance and gives you insights about how ahead or behind you are in your Courses.

<hr>

<img src="https://elvistony.github.io/assets/repo/ondot/app_ss_b.jpg" width=300px> 

### Get an Overview of Your Presence

<hr>

<img src="https://elvistony.github.io/repo/ondot/app_ss_a.jpg" width=300px> 

### Get Insights into Each Course
